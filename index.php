<?php
session_start();
include "models/Book.php";
include "models/Cart.php";
include "lib/func.inc.php";

$cart = new Cart();

for ($i = 1; $i <= count(Book::getAll()); $i++) {
    $submitIt = "submit$i";
    if (isset($_POST[$submitIt])) {
        $book = Book::get($_POST['id' . $i]);
        $cart->add($book, $_POST['select' . $i]);
        header("Location: shoppingCart.php");
    }
    unset($_POST[$submitIt]);
    unset($_POST['select' . $i]);
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
<!--        <script src="https://kit.fontawesome.com/4963e1ecd1.js" crossorigin="anonymous"></script>-->
    <title>Warenkorb</title>

</head>

<body>
<div class="container-fluid">
    <form action="index.php" method="post">
    <div class="row">
        <div class="col-sm-4"></div>

        <div class="col-sm-4">
            <div class="row mt-5">
                <div class="col-sm-6">
                    <h1 style="font-weight: bold">Bücher</h1>

                </div>
                <div class="col-sm-6">
                    <input type="button"
                           class="btn btn-primary btn-lg float-right"
                           id="shoppingCart"
                           value="ShoppingCart: <?php echo sizeof($cart->getList()) ?>"
                           onclick="location.href='shoppingCart.php'"
                    >
<!--                                        <button class="btn btn-primary btn-lg float-right"-->
<!--                                                id="shoppingCart"-->
<!--                                        ><i class='fas fa-shopping-cart'> Warenkorb-->
<!--                    --><?php //echo sizeof($cart->getList()) ?><!--</button>-->
                </div>
            </div>

            <div class="row">
                <form action="shoppingCart.php" method="post">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Titel</th>
                            <th scope="col">Preis</th>
                            <th scope="col">Menge</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $allBooks = Book::getAll();
                        $i = 1;
                        $cartList = $cart->getList();
                        foreach ($allBooks as $book) {
                            foreach ($cartList as $list) {
                                if ($list->getBook()->getId() == $book->getId()){
                                    $book->setStock($book->getStock() - $list->getAmount());
                                }
                            }
                            echo "<input type='hidden' name='id$i' id='$i' value=" . $book->getId() . ">
                        <tr>
                            <td> " . $book->getTitle() . "</td>
                            <td> " . "€" . $book->getPrice() . "</td>
                            <td> <select name='select$i' id='select$i'>" . dropDown($book->getStock()) . "</select></td>
                            <td> <button type='submit' 
                                    name='submit$i'  
                                    id='submit$i'  
                                    >hinzufügen</button></td>
                                                            
                        </tr>";
                            $i++;
                        }
                        ?>
                        </tbody>
                    </table>
                </form>
            </div

        </div>
    </div>
    </form>
</div>

</body>
</html>