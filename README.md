# Warenkorb


# Inhaltsverzeichnis
- [Warenkorb](#warenkorb)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Ausgangslage](#ausgangslage)
  - [Problemstellung](#problemstellung)
  - [Ziel](#ziel)
  - [Das erwartete Produkt](#das-erwartete-produkt)
  - [Zusatzfunktionen](#zusatzfunktionen)
  - [Wireframes](#wireframes)
- [Lösungsansatz](#lösungsansatz)
  - [Projekt mit Ordnerstruktur ersellen](#projekt-mit-ordnerstruktur-ersellen)
  - [Formular erstellen & Eingabefelder definieren](#formular-erstellen--eingabefelder-definieren)
  - [Clientseitige validierung](#clientseitige-validierung)
  - [Serverseitige Validierung](#serverseitige-validierung)
  - [Book-Klasse](#book-klasse)
    - [Funktionen](#funktionen)
  - [Cart-Klasse](#cart-klasse)
    - [Funktionen](#funktionen-1)
  - [CartItem-Klasse](#cartitem-klasse)
    - [Funktionen](#funktionen-2)
    - [Startseite - index.php](#startseite---indexphp)
  - [Warenkorb - shoppingCart.php](#warenkorb---shoppingcartphp)
- [Implementierung / Wichtigsten Codesegmente](#implementierung--wichtigsten-codesegmente)
  - [Package lib](#package-lib)
    - [**PHP file func.inc.php**](#php-file-funcincphp)
  - [Package models](#package-models)
    - [**Book Klasse**](#book-klasse-1)
      - [**public static function getAll()**](#public-static-function-getall)
      - [**public static function get($id)**](#public-static-function-getid)
    - [**Cart Klasse**](#cart-klasse-1)
      - [**private function loadcookie()**](#private-function-loadcookie)
      - [**private function saveCookie()**](#private-function-savecookie)
      - [**public function add($book, $count)**](#public-function-addbook-count)
      - [**public funtion remove($id)**](#public-funtion-removeid)
      - [**public function getList()**](#public-function-getlist)
    - [**CartItem Klasse**](#cartitem-klasse-1)
  - [Index.php](#indexphp)
    - [**Wichtige Segmente:**](#wichtige-segmente)
  - [shoppingCart.php](#shoppingcartphp)
    - [Wichtigen Segmente**](#wichtigen-segmente)
- [Testprotokoll](#testprotokoll)




# Ausgangslage

## Problemstellung

Ein lokaler Buchhändler möchte seine Bücher zukünftig online über einen einfachen Webshop vertreiben. Er hat sich bereits einige Gedanken gemacht und zwei Wireframes erstellt.

## Ziel

Dein Chef hat dich beauftragt einen Prototyp für die Warenkorbfunktionalität des Webshops zu erstellen. Der Warenkorb soll clientseitig gespeichert werden und somit bei erneutem Besuch wieder angezeigt werden. Die Testdaten werden als JSON-String zur Verfügung gestellt. Eine Benutzerauthentifizierung ist nicht notwendig.

## Das erwartete Produkt

Der Prototyp sollte mindestens folgendes umfassen:
 + Startseite / Shop
   + Anzeige aller Artikel mit Name, Preis und Verfügbarkeit (Lagerstand)
   + Hinzufügen eines Artikels mit gewünschter Menge zum Warenkorb
 + Warenkorbseite
   + Anzeige der aktuell ausgewählten Artikel inklusive Gesamtpreis
   + Entfernung von Artikeln aus dem Warenkorb
 + Navigation zwischen Startseite und Warenkorb
 + Vollständige Trennung von Darstellung und Geschäftslogik
 + Objektorientierte Programmierung
   + **Erstelle ein Klassendiagramm**
   + Hinweis: 3 Klssen werden benötigt
     + Book
       + public static function getAll()
       + public static function get($id)
     + Cart
       + private function loadCookie()
       + private function saveCookie()
       + public function add($book, $count)
       + public function remove($id)
     + CartItem

## Zusatzfunktionen

+ Die auswählbare Menge darf nicht größer als der Lagerstand sein
+ Hinzufügen von Artikeln in den Warenkorb reduziert die Auswahlmöglichkeit in den Selectboxen um die entsprechende Anzahl
+ Bearbeiten der Artikelmenge im Warenkorb

## Wireframes

Artikel hinzufügen

![Artikel](Mockups/Artikel.png)

Warenkorb bearbeiten

![Cart](Mockups/Cart.png)



# Lösungsansatz

## Projekt mit Ordnerstruktur ersellen

+ css
  + bootstrap.css
+ images
+ js
  + bookdata.json
+ lib
  + func.inc.php
+ models
  + Book.php
  + Cart.php
  + CartItem.php
+ index.php
+ shoppingCart.php

<br>

## Formular erstellen & Eingabefelder definieren 

+ HTML-Formular für die Übermittlung der Daten erstellen.
+ Drop-Down Input Feld für die Auswahl der Stückzahl.
+ Übermittlung erfolgt mittels HTTP-Post (action & method)
+ Submit Button zur Formularübermittlung
+ Input Feld um später um Warenkorb Stückzahl zu verändern und löschen

<br>

## Clientseitige validierung

Die clientseitige Validierung verhindert fehlerhafte Benutzereingaben und ermöglicht eine sofortige Rückmeldung während der Eingabe.

<br>

## Serverseitige Validierung

Die serverseitige Validierung verhindert die Weiterverarbeitung und Speicherung fehlerhafter Daten.

<br>

## Book-Klasse
Erstellung eines Buch-Objektes.

### Funktionen
+ public static function getAll()
+ public static function get($id)
  
<br>


## Cart-Klasse

Erstellung eines Cart-Objektes welcher unseren Einkaufswagen darstellt.

### Funktionen

+ private function loadCookie()
+ private function saveCookie()
+ public function add($book, $count)
+ public function remove($id)

<br>

## CartItem-Klasse 

Erstellung einer CartItem-Klasse welche uns die ausgewählten Bücher und deren Menge, welche gekauft werden sollen, liefert. 

### Funktionen 

Getter- und Setter Methoden für das Buch-Objekt und die Menge der Bücher.

### Startseite - index.php

+ Darstellung der Bücher mit der auf Lager verfügbaren Stückzahl.
+ Button zum hinzufügen des Buches und wechseln in den Warenkorb.
+ Hinzufügen von Artikeln in den Warenkorb. Stückzahl ist beschränkt durch die Auswahlmöglichkeit in den Selectboxen.
  
## Warenkorb - shoppingCart.php

+ Darstellung des Warenkorbes mit den ausgewählten Büchern.
+ Möglichkeiten zur Bearbeitung der Stückzahl bzw. zum löschen des Artikels.

<br>

# Implementierung / Wichtigsten Codesegmente

In diesem Bereich werden die wichtigsten Codeteile aller Klassen beschrieben und erklärt.

## Package lib

### **PHP file func.inc.php**

In dem PHP file func.inc.php befindet sich nur eine Funktion namens dropDown. Die **function dropDown($stock)** hat einen Parameter und prüft ob das "Lager" bzw. die Menge 0 ist. Ist dies der Fall wird Ausverkauft angezeigt. Andernfalls wird eine for schleife durchlaufen und gezählt wieviele Bücher vohranden sind und diese dann als Zahlen in einem Dropdown Menü aufgelistet bzw. in der Funktion werden diese zurückgegeben.


```php
function dropDown($stock)
{
    $options = '';
    if ($stock == 0) {
        $options .= "<option id='count' value='0'>Ausverkauft</option>";
    } else {
        for ($i = 0; $i <= $stock; $i++) {
            $options .= "<option id='count' value='$i'>" . $i . "</option>";
        }
    }
    return $options;
}
```

**Darstellung**

![Dropdown](Mockups/Dropdown.png)

<br>

## Package models

### **Book Klasse**

In der Book Klasse werden die entsprechenden Datenfelder deklariert, ein Konstruktor mit Parameter definiert, Getter und Setter, sowie zwei wichtige Funktionen implementiert.

#### **public static function getAll()**
  
  Die statsiche Funktion **getAall()** gibt alle vorhandenen Bücher des Json files aus und ist parameterlos. In dieser Funktion wird das gegebene Json file decodiert mit Hilfe der **json_decode()** Funktion, die als Parameter die Funktion **file_get_contents()** hat, welche die Daten des Json files ausliest mit Hilfe der mitgegeben Parameter. Diese Daten werden anschließend in die Variable **data** gespeichert. Weiteres wird ein neues **Array[]** erstellt und in die Variable **allBooks** gespeichert. Dann wird eine **foreach Schleife** durchlaufen und alle Daten des Json files werden mit der Methode **array_push()** in das neue Array gepushed. Mit den Parametern wird einmal das neue Array angegebn und es wird ein neues Book Objekt mit allen Parametern erstellt. Zum Schluss wid das Array **allBooks** zurückgegeben.

Parameter der Funktion **file_get_contents()**:
  + Den Filenamen **"js/bookdata.json"**
  + Boolean auf false (optional)
  
<br>

```php
  public static function getAll(){
        $data = json_decode(file_get_contents("js/bookdata.json"), false);
        $allBooks = array();
        foreach ($data as $book){
            array_push($allBooks, new Book($book->id, $book->title, $book->price, $book->stock));
        }
        return $allBooks;
    }
```

<br>

  
#### **public static function get($id)**

Die statische Funktion **get($id)** gibt das gewünschte Buch nach der Id aus und hat einen Paramter **id**. Zuerst werden alle Daten in das Array[] **allBooks** mit dem aufruf **self::getAll()** gespeichert. **self** wird verwendet um Zugang zu der statsichen Methode **getAll** zu erlangen. Anschließend wird eine **foreach Schleife** durchlaufen und geprüft ob die **Id** des Buches der mitgegeben **Id** entspricht und falls dies zutrifft wird jenes zurückgegeben. Andernfalls wird **NULL** zurückgegeben.

**Wichtig:**

+ **self** (Für Zugriff auf statische oder Klassenvariablen oder Methoden)
+ **this** (Für Zugriff auf nicht-statische oder Objektvariablen oder Methoden)


```php
    public static function get($id){
        $allBooks = self::getAll();
        foreach ($allBooks as $book){
            if($book->getId() == $id){
                return $book;
            }
        }
        return null;
    }
```
<br>

### **Cart Klasse**

Die Cart Klasse beinhaltet/verwendet die **CartItem.php** Klasse, ein Datenfeld namens **list**, einen Konstruktor der uns den **Cookie** lädt, sowie private/public Funktionen um ein Cookie zu laden & speichern, ein Buch in den Einkaufswagen hinzuzufügen & zu entfernen und eine Funktion die eine Liste von ausgewählten Büchern ausgibt mithilfe der gesetzten Cookies.

#### **private function loadcookie()**

In dieser Funktion wird zuerst geprüft ob das **Cookie** gesetzt ist. Trifft dies zu wird in die Variable **list unserialized()** das heißt, dass die Datenmenge wieder in PHP-Werte umgewandelt werden.

**Cookie:**
+ Name = 'allCookies'
+ Value = serialize($this->list) um einen String aus den Array zu erzeugen.
+ Dauer/Zeit = time() + 3600 um die Lebensdauer des Cookies festzulegen.

```php
  private function loadCookie(){

        if(isset($_COOKIE['allCookies'])){            
            $this->list = (unserialize($_COOKIE['allCookies']))
        }
  }
```

<br>

#### **private function saveCookie()**

Diese Funktion wird verwendet um einen Cookie zu speichern.

```php
   private function saveCookie(){
        setcookie('allCookies', serialize($this->list), time() + 3600);
    }
```
<br>

#### **public function add($book, $count)**

Diese Funktion dient um ausgewählte Bücher in den Einkaufswagen hinzuzufügen. Sie hat zwei Parameter namens **book & count**.
Zuerst wird die Funktion **loadcookie()** in das Array[] **list** gespeichert. Anschließend wird mit einer **foreach Schleife** das Array[] durchlaufen und alle gewählten Bücher, die mit der Liste Übereinstimmen dort hineingegeben und gespeichert. Zum Schluss wird mit der Mehtode **array_push()** ein **neues CartItem** Objekt mit dem gewählten Buch und Anzahl erzeugt und in das **list** Array[] gespeichert. Der Cookie wird ebenfalls gespeichert.

```php
    public function add($book, $count){

        $this->loadCookie();
        foreach ($this->list as $item){
            if($item->getBook()->getId() == $book->getId()){
                $item->setAmount($item->getAmount() + $count);
                $this->saveCookie();
                return;
            }
        }
        array_push($this->list, new CartItem($book, $count));
        $this->saveCookie();
    }
```


#### **public funtion remove($id)**

Diese Funktion dient dazu, ausgewählte Bücher, die sich im Einkaufswagen befinden zu entfernen. Sie hat einen Parameter names **id**. Auch hier wird wieder zuerst die Funktion **loadCookie()** in das Array[] **list** gespeichert. Darunter befindet sich eine lokale Variable namens **i** die mit 0 initialisiert wird. Anschließend wird mit einer **foreach Schleife** das Array[] **list** durchlaufen und geprüft ob die Id des gewählte Buches mit der gesuchten Id übereinstimmt. Ist dies der Fall wird diese mit Hilfe der Methode **unset()** zerstört/entfernt. Dann wird die lokale Variable **i** inkrementiert, die Werte des Arrays[] **list** mit der Methode **array_values()** zurückgegeben und aktualisiert und der Cookie wird gespeichert.


```php
    public function remove($id){

        $this->list = $this->loadCookie();
        $i = 0;
        foreach ($this->list as $cartItem){
            if($cartItem->getBook()->getId() == $id){
                unset($this->list[$i]);
            }
            $i++;
        }
        $this->list = array_values($this->list);
        $this->saveCookie();
    }
```

<br>

#### **public function getList()**

Diese Funktion liefet eine liste mit den Cookies.

```php
 public function getList(){
        return $this-list;
    }
```

### **CartItem Klasse**

Diese Klasse dient als Hilfsklasse um gewählte Bücher in den Einkaufswagen zu legen. Sie besitzt zwei Datenfelder namens **book & amount** sowie einen Konstruktor mit diesen Datenfeldern als Parameter, als auch Getter & Setter. 

**Datenfelder / Konstruktor**

```php
class CartItem{

    private $book;
    private $amount;

    public function __construct($book, $amount){
        $this->book = $book;
        $this->amount = $amount;
    }
```

## Index.php

In der index.php wird die Liste mit allen Büchern deren Titel, Preis und Anzahl angezeigt. Man kann Bücher in den Einkaufswagen legen und sieht wieviel Stk. noch vorhanden sind bzw. Ausverkauft sind.

### **Wichtige Segmente:**

Im ersten Teil der index.php wird die mit **session_start();** festgelegt das Daten während einer Folge von Aufrufen der Website zu verwenden.
Weiteres werden alle PHP files die benöigt werden mit **include** eingebunden. Weiteres wird ein neues **Cart Objekt** erstellt und in die Variable **cart** gespeichert. 


```php
session_start();
include "models/Book.php";
include "models/Cart.php";
include "lib/func.inc.php";

$cart = new Cart();
```
<br>

In diesem Segment wird mit einer **for Schleife** die Json Datei durchlaufen. Dafür wird die Mehtode **count()** verwendet welche die Anzahl der Emelente in einem Array[] zurück gibt. Als Parameter wird die **getAll()** Methode der Klasse Book mitgegeben, daher der Aufruf **Book::getAll()**. Darunter befindet sich die Variabel **submitIt**, welche den String **"submit"** speichert. Weiteres wird geprüft ob der Parameter **submitIt** existiert bzw. gesetzt ist. Ist dies der Fall wird in die Variable **book** das entsprechende Buch mit der id gespeichert, mit Hilfe des Aufrufs **Book::get($_POST['id' . $i]);** . In die Variable **cart** wird mit der **add(book, $_POST['select' . $i]);** Methode das gewählte Buch hinzugefügt.
Diese Objekte werden dann mit der **header** Methode an die shoppingCart.php Seite weitergeleitet. Anschließend werden mit der Methode **unset()** die globalen Varaiblen zurückgesetzt.

```php

for ($i = 1; $i <= count(Book::getAll()); $i++) {
    $submitIt = "submit$i";
    if (isset($_POST[$submitIt])) {
        $book = Book::get($_POST['id' . $i]);
        $cart->add($book, $_POST['select' . $i]);
        header("Location: shoppingCart.php");
    }
    unset($_POST[$submitIt]);
    unset($_POST['select' . $i]);
}
```

<br>

Hier werden alle Bücher aufgelistet und angezeigt. Erneut wird mit dem Aufruf **Book::getAll();** über die Klasse **Book** alle Bücher in das Array[] **allBooks** gespeichert. Darunter wird in das Array[] **cartList** die Methode **getList()** gespeichert welche die verwendeten Cookies auflistet. Anschließend werden zwei **foreach Schleifen** durchlaufen. Die Erste speichert alle Bücher in die Variable **book**, die Zweite speichert alle ausgewählten Bücher. Danach wird geprüft ob sich die ausgewählen Bücher in dem **allBooks** Array[] befinden, falls das zutrifft werden jene Bücher ausgegeben.

```php
<?php
$allBooks = Book::getAll();
 $cartList = $cart->getList();
 foreach ($allBooks as $book) {
    foreach ($cartList as $list) {
       if ($list->getBook()->getId() == $book->getId()){
           $book->setStock($book->getStock() - $list->getAmount());
        }
    }
?>
```

<br>


## shoppingCart.php

In der shoppingCart.php werden die ausgewählten Bücher, die Menge, der Preis pro Stk. und der Gesamtpreis angezeigt. Weiteres ist es möglich ausgewählte Bücher wieder zu entfernen.

### Wichtigen Segmente**

In diesem Bereich wird zuerst geprüft ob das **Cookie** gesetzt ist. Trifft dies zu wird die Liste der gewählten Bücher in das **cartList** Array[] gespeichert. Die Variable **totalPrice**, welche den Gesamtpreis berechnet, wird mit 0 deklariert. Anschließend wird mit einer **foreach Schleife** das Array[] **cartList** durchlaufen und alle gewählten Bücher werden ausgegeben.
Danach wird geprüft ob die Variable **submit** gesetzt ist. Trifft dies zu wird das gewählte Buch nach der **Id** entfernt. Die **header()** Methode verweist auf die aktuelle Seite shoppingCart.php.

```php
<?php

if (isset($_COOKIE['allCookies'])) {
$cartList = $cart->getList();
$i = 1;
$totalPrice = 0;
foreach ($cartList as $item) {
$book = $item->getBook();
$amount = $item->getAmount();
$price = $book->getPrice() * $amount;
$submit = "submit$i";

 if (isset($_POST[$submit])) {
  $cart->remove($book->getId());
   header("Location: shoppingCart.php");
  }
}
?>
```
# Testprotokoll 

Erster Aufruf der index.php Seite. Session wird gestartet und Cookies werden gesetzt. 

![Cookies1](images/cookie1.png) 

Anzeige der Bücherliste mit Titel, Preis, Menge. Über DropDown lässt sich die Menge festlegen. Ist das Buch ausverkauft wird dies sofort angezeigt. Durch drücken des "hinzufügen" Buttons wird das Buch in den Einkaufswagen gelegt. 
Über den Button "ShoppingCart" gelangt man in den Warenkorb. Außerdem wird die Anzahl der Bücher nach Titel, die sich bereits im Warenkorb befinden, angezeigt.

![Anzeige-Bücher](images/index.png) 

Über das DropDown Menü wird die Menge festgelegt und mit "hinzufügen" in den Warenkorb gelegt. 

![Auswahl1](images/auswahl1.png) 

![Cookies2](images/cookie2.png) 

![Header1](images/header1.png)

Das Buch und die Menge wurden von der index.php übernommen und auf der shoppingCart.php angezeigt.

![warenkorb1](images/warenkorb1.png) 

Durch klicken auf "weiter einkaufen" gelangt man wieder auf die index.php. Da sich keine Bücher mit dem Titel Y-Solowarm mehr auf Lager befinden wird "Ausverkauft" angezeigt.

![index2](images/index2.png) 

Es wird ein weiteres Buch ausgewählt und zum Warenkorb hinzugefügt. 

![auswahl2](images/auswahl2.png)

![header2](images/header2.png)

![header3](images/header3.png)

![cookie3](images/cookie3.png)

Bücher werden im Warenkorb angezeigt. 

![warenkorb2](images/warenkorb2.png)

Durch drücken des "Entfernen" Buttons wird das Buch aus dem Einkaufswagen entfernt. 

![warenkorb3](images/warenkorb3.png) 

![header4](images/header4.png)

![header5](images/header5.png)