<?php


class Book {

    private $id = '';
    private $title ='';
    private $price = '';
    private $stock = '';


    public function __construct ($id, $title, $price, $stock){
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->stock = $stock;
    }

    public static function getAll(){
        $data = json_decode(file_get_contents("js/bookdata.json"), false);
        $allBooks = array();
        foreach ($data as $book){
            array_push($allBooks, new Book($book->id, $book->title, $book->price, $book->stock));
        }
        return $allBooks;
    }

    public static function get($id){
        $allBooks = self::getAll();
        foreach ($allBooks as $book){
            if($book->getId() == $id){
                return $book;
            }
        }
        return null;
    }


    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param string $newStock
     */
    public function setStock($newStock)
    {
        $this->stock = $newStock;
    }

}