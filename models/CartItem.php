<?php

class CartItem{

    private $book;
    private $amount;

    public function __construct($book, $amount){
        $this->book = $book;
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param mixed $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $newAmount
     */
    public function setAmount($newAmount)
    {
        $this->amount = $newAmount;
    }
}
