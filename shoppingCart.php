<?php
session_start();
include "models/Book.php";
include "models/Cart.php";

$cart = new Cart();

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Warenkorb</title>

</head>

<body>
<div class="container-fluid">
    <form action="shoppingCart.php" method="post">
        <div class="row mt-5">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <h1>Einkaufswagen</h1></div>
            <button type="button"
                    class="btn btn-primary btn-lg"
                    value="Weiter einkaufen"
                    onclick="location.href = 'index.php'">weiter einkaufen
            </button>
        </div>

        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <table class="table table-striped">
                    <thead>
                    <tr class="text-center">
                        <th scope="col">Titel</th>
                        <th scope="col">Preis/Stk.</th>
                        <th scope="col">Menge</th>
                        <th scope="col">Gesamtpreis</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if (isset($_COOKIE['allCookies'])) {
                        $cartList = $cart->getList();
                        $i = 1;
                        $totalPrice = 0;
                       // var_dump($cartList);
                        foreach ($cartList as $item) {
                            $book = $item->getBook();
                            $amount = $item->getAmount();
                            $price = $book->getPrice() * $amount;
                            $submit = "submit$i";

                            if (isset($_POST[$submit])) {
                                $cart->remove($book->getId());
                                header("Location: shoppingCart.php");
                            }


                           echo "
                       <tr class='text-center'>
                           <td> " . $book->getTitle() . "</td>
                            <td> " . "€ " . $book->getPrice() . " </td>
                            <td> "  . $amount . " </td>
                           <td> " . "€ " . $price . " </td>
                            <td><button type='submit'
                                        name='submit$i'
                                        id = 'submit$i'
                                        value='submit$i'
                             >Entfernen</button></td>
                            </tr>";
                              $i++;
                            $totalPrice += $price;
                        }
                        echo "
                        <tr>
                        <td> " . " Gesamtpreis: " . "€ " . $totalPrice ."</td>
                        </tr>
                    ";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
</body>
</html>

